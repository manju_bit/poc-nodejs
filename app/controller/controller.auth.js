/*jshint esversion: 6 */

/* Listing */
exports.oauth0Login = (req, res) => {
  try {
    console.log('====================================');
    console.log('Successfully Validated');
    console.log('====================================');
  } catch (error) {
    res.json('Something went wrong');
  }
};

exports.oauth0LoginCallback = (req, res, next) => {
  try {
    res.json({ id_token: req.authInfo.id_token, token_type: req.authInfo.token_type, expires_in:  req.authInfo.expires_in});
  } catch (error) {
    res.json('Something went wrong');
  }
};

exports.secured = (req, res, next) => {
    if (req.user) {
        return next();
    }
    req.session.returnTo = req.originalUrl;
    res.redirect('/auth/auth0');
};
