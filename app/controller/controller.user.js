/*jshint esversion: 6 */
const jwt = require('jsonwebtoken');
const User = require('../model/model.user');
const request = require('request');
const config = require('../../config/config');
var constant = require('../../config/constant');

exports.UserSignup = (req, res) => {
    const authSignupUrl = 'https://' + config.get('authentication.oauth0.domain') + '/dbConnections/signup';
    try {
        const user = new User({
            name: req.body.name,
            email: req.body.email,
            isActive: true
        });
        const authData = {
            "client_id": config.get('authentication.oauth0.clientID'),
            "email": user.email,
            "password": req.body.password,
            "connection": "Username-Password-Authentication",
            "username": user.name,
            "given_name": user.name,
            "family_name": user.name,
            "name": user.name
        };
        const options = {
            uri: authSignupUrl,
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                Accept: 'application/json'
            },
            form: authData
        };
        request.post(options, function (err, authResult) {
            if (authResult.statusCode === 400) {
                res.status(400).json({ Error: { message: constant.getResponse(constant.errorMsg.SIGNUP_USE_EMAIL) } });
            }
            else if (authResult.statusCode === 200) {
                user.save(function (error, result) {
                    if (error) {
                        res.json({ Error: constant.getResponse(constant.errorMsg.USER_EXIST) });
                    }
                    else {
                        res.status(200).json({ User: result });
                    }
                });
            } else if (err) {
                res.json({ Error: constant.getResponse(constant.errorMsg.EXCEPTION) });
            }
            else {
                res.json({ Error: constant.getResponse(constant.errorMsg.EXCEPTION) });
            }
        });
    }
    catch (error) {
        res.status(500).json({ Error: constant.getResponse(constant.errorMsg.EXCEPTION) });
    }
};
//changing password
exports.changePassword = (req, res) => {
    const authChangePasswordUrl = `https://${config.get('authentication.oauth0.domain')}/dbconnections/change_password`;
    try {
        User.findOne({ email: req.body.email })
            .exec(function (err, result) {
                if (err) {
                    res.status(500).json({ Error: constant.getResponse(constant.errorMsg.EXCEPTION) });
                }
                else if (result === null) {
                    res.json({ Error: constant.getResponse(constant.errorMsg.EMAIL_INVALID) });
                }
                else {
                    const authData = {
                        "client_id": config.get('authentication.oauth0.clientID'),
                        "email": req.body.email,
                        "connection": "Username-Password-Authentication"
                    };

                    const options = {
                        uri: authChangePasswordUrl,
                        method: 'POST',
                        headers: {
                            "Content-Type": "application/json",
                            Accept: 'application/json'
                        },
                        form: authData
                    };

                    request.post(options, function (err, authResult) {
                        // console.log("err===" + err);
                        // console.log("success====" + authResult);
                        if (authResult.statusCode === 400) {
                            res.status(400).json({ Error: { message: authResult.body } });
                        }
                        else if (authResult.statusCode === 200) {
                            res.status(200).json({ success: { message: authResult.body } });
                        }
                    });
                }
            });
    } catch (error) {
        res.status(500).json({ Error: constant.getResponse(constant.errorMsg.EXCEPTION) });
    }
};
/* Listing search paginatoin*/
exports.UserList = (req, res) => {
    try {
        // let value = (req.params.id)
        var inputParam = {};
        if (req.body.name && req.body.name != "") {
            inputParam.$or = [
                // { State: new RegExp(".*" + req.body.State + ".*", 'i') },
                { name: new RegExp(".*" + req.body.name + ".*", 'i') },
            ];
        }

        /*       var pageNo = parseInt(req.body.pageNo);
              var size = parseInt(req.body.size);
              var query = {};
              if (pageNo < 0 || pageNo === 0) {
                  res.json({ "error": true, "message": "invalid page number, should start with 1" });
                  return res.json(response);
              }
              
              query.skip = size * (pageNo - 1);
              query.limit = size;
              User.count({}, function (err, totalCount) {
                  if (err) {
                      res.json({ "error": true, "message": "Error fetching data" });
                  } */

        var limit = constant.errorMsg.LIST_LIMIT;
        if (req.body.pageSize >= 5) {
            limit = req.body.pageSize;
        }
        var skip = 0;
        if (req.body.pageNo) {
            skip = (limit * req.body.pageNo) - limit;
        }


        User.find(inputParam, { _id: 0 }).sort({ dateUpdated: 1 }).skip(skip).limit(limit)
            .exec(function (error, result) {
                if (error) {
                    console.log(error);
                    res.status(404).json({ Error: constant.getResponse(constant.errorMsg.LIST_FAILED) });
                }
                else if (result.length > 0) {
                    res.status(200).json({ Users: result });
                }
                else {
                    res.status(404).json({ Error: constant.getResponse(constant.errorMsg.NO_DATA) });
                }

            });

    }
    catch (error) {
        res.status(500).json({ Error: constant.getResponse(constant.errorMsg.EXCEPTION) });
    }
};
/* update */
exports.UserUpdate = (req, res) => {
    try {
        var value = (req.headers.userid);
        var input = req.body;
        if (!input.name) {
            res.status(400).json({ Error: constant.getResponse(constant.errorMsg.FIELD_REQ) });
        }
        else {
            User.findOne({ userid: value })
                .exec(function (error, result) {
                    // res.json(result)
                    if (result == null) {
                        res.status(404).json({ Error: constant.getResponse(constant.errorMsg.NO_DATA) });
                    }
                    else if (result.role === 1) {
                        User.updateOne({ userid: input.userid }, {
                            $set: {
                                name: input.name,
                                isActive: input.isActive || false,
                                role: input.role || 0
                            }
                        }, { new: true })
                            .exec(function (error, result) {
                                if (error) {
                                    res.json({ Error: constant.getResponse(constant.errorMsg.USER_NOT_UPDATE) });
                                }
                                else if (result.n === 1) {
                                    /*  if (result.nModified === 0) {
                                         res.json({ message: 'no changes' });
                                     }
                                     else { */
                                    // }
                                    res.status(200).json({ success: constant.getResponse(constant.errorMsg.UPDATE_USER) });

                                }
                            });
                    }
                    else if (result.role === 0) {
                        res.status(401).json({ Error: constant.getResponse(constant.errorMsg.AUTH_ERROR) });
                    }
                });
        }
    }
    catch (error) {
        res.json({ Error: constant.getResponse(constant.errorMsg.EXCEPTION) });
    }
};
/* DELETE */
exports.UserDelete = (req, res) => {
    try {
        let value = (req.params.userid);
        User.findOne({ userid: value })
            .exec(function (error, result) {
                // res.json(result)
                if (result == null) {
                    res.status(404).json({ Error: constant.getResponse(constant.errorMsg.NO_DATA) });
                }
                else if (result.role === 1) {
                    User.findOneAndDelete({ userid: req.body.userid })
                        .exec(function (error, result) {
                            if (error) {
                                res.json({ Error: constant.getResponse(constant.errorMsg.USER_NOT_DELETED) });
                            }
                            else if (result === null) {
                                res.json({ message: "no record to delete" });
                            }
                            else {
                                res.status(200).json({ success: constant.getResponse(constant.errorMsg.DELETE_USER) });

                            }
                        });
                }
                else if (result.role === 0) {
                    res.status(401).json({ Error: constant.getResponse(constant.errorMsg.AUTH_ERROR) });
                }
            });
    }
    catch (error) {
        res.status(500).json({ Error: constant.getResponse(constant.errorMsg.EXCEPTION) });
    }
};
/* LOGIN */
exports.Userlogin = (req, res) => {
    /*const authLoginUrl = 'https://' + config.get('authentication.oauth0.domain') +'/authorize'?
    response_type='token'& client_id=config.get('authentication.oauth0.clientID')&
    connection=config.get('authentication.oauth0.dbConnection')&redirect_uri='http://localhost:5000//auth/auth0/callback';

    const options = {
        uri: authLoginUrl,
        method: 'GET',
        headers: {
            "Content-Type": "application/json",
            Accept: 'application/json'
        }
    };*/
    try {
        User.findOne({ email: req.body.email })
            .exec(function (err, result) {
                if (err) {
                    res.status(500).json({ Error: constant.getResponse(constant.errorMsg.EXCEPTION) });
                }
                else if (result === null) {
                    res.json({ Error: constant.getResponse(constant.errorMsg.LOGIN_FAILED) });
                }
                else {
                    const options = {
                        method: 'POST',
                        uri: `https://${config.get('authentication.oauth0.domain')}/oauth/token`,
                        headers: { 'content-type': 'application/json' },
                        body: JSON.stringify({
                            client_id: config.get('authentication.oauth0.clientID'),
                            client_secret: config.get('authentication.oauth0.clientSecret'),
                            username: req.body.email,
                            password: req.body.password,
                            grant_type: 'http://auth0.com/oauth/grant-type/password-realm',
                            realm: 'Username-Password-Authentication',
                            // audience: 'c5Qv8Y9eCbnDXiOaqS45CR9lmuIYrLGo',
                            audience: 'https://dev-vibrant.auth0.com/api/v2/',
                            scope: 'openid',
                            responseType: 'token id_token'
                        })
                    };
                    request.post(options, function (err, authResult) {
                        if (authResult.statusCode === 200) {
                            const accessToken = {
                                acessToken: JSON.parse(authResult.body).access_token,
                                idToken: JSON.parse(authResult.body).id_token,
                                userid: result._id,
                                role: result.role
                            };
                            res.json(accessToken);
                            // res.json( JSON.parse(authResult.headers.x-auth0-requestid ) );

                        } else if (authResult.statusCode === 403) {
                            console.log(authResult);
                            res.json({ Error: JSON.parse(authResult.body).error_description });
                        }
                        else {
                            res.status(400).json({ Error: JSON.parse(authResult.body).error });
                        }
                    });
                }
            });
    }
    catch (Error) {
        res.status(500).json({ Error: constant.getResponse(constant.errorMsg.EXCEPTION) });
    }
};

//Active and inActive user
exports.UserActive = (req, res) => {
    const users = {
        active: [],
        inActive: []
    };
    try {
        User.find({ isActive: true }, { _id: 0 })
            .exec(function (error, result) {
                if (error) {
                    res.status(404).json({ Error: constant.getResponse(constant.errorMsg.LIST_FAILED) });
                }
                else {
                    User.find({ isActive: false }, { _id: 0 })
                        .exec(function (error, result1) {
                            if (error) {
                                res.status(404).json({ Error: constant.getResponse(constant.errorMsg.LIST_FAILED) });
                            } else {
                                users.active = result;
                                users.inActive = result1;
                                res.json({ users: users });
                            }
                        });
                }
            });
    }
    catch (Error) {
        res.status(500).json({ Error: constant.getResponse(constant.errorMsg.EXCEPTION) });

    }
};

//search
exports.UserSearch = (req, res) => {
    try {
        var inputParam = {};

        if (req.body.name && req.body.name != "") {
            inputParam.$or = [
                // { State: new RegExp(".*" + req.body.State + ".*", 'i') },
                { name: new RegExp(".*" + req.body.name + ".*", 'i') },
            ];

        }
        console.log(inputParam);

        User.find(inputParam).exec(function (err, result) {
            //   console.log("object", object);
            if (err) {
                res.status(404).json({ Error: constant.getResponse(constant.errorMsg.LIST_FAILED) });
            }
            else {
                // console.log(result);
                res.status(200).json({ Users: result });
            }
        });
    }
    catch (err) {
        res.status(500).json({ Error: constant.getResponse(constant.errorMsg.EXCEPTION) });

    }
};
//Active and inActive user without Array
/* exports.UserActive = (req, res) => {
    try {
        User.find({ isActive: true }, { _id: 0 })
            .exec(function (error, result) {
                if (error) {
                    res.status(404).json({ Error: constant.getResponse(constant.errorMsg.LIST_FAILED) });
                }
                else {
                    User.find({ isActive: false }, { _id: 0 })
                        .exec(function (error, result1) {
                            if (error) {
                                res.status(404).json({ Error: constant.getResponse(constant.errorMsg.LIST_FAILED) });
                            } else {
                                res.json({Active :result .concat( {inActive:  result1})});
                            }

                        });
                }
            });
    }
    catch (Error) {
        res.status(500).json({ Error: constant.getResponse(constant.errorMsg.EXCEPTION) });

    }
}; */
/* exports.UserSingup = (req, res) => {
    const authSignupUrl = 'https://' + config.get('authentication.oauth0.domain') + '/' + 'Username-Password-Authentication' + '/signup';

    try {
        var user = new User({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            isActive: true
        });

        const authData = {
            connection: "Username-Password-Authentication",
            client_id: config.get('authentication.oauth0.clientID'),
            name: user.name,
            email: user.email,
            password: user.password,
            username: user.name,
            given_name: user.name,
            family_name: user.name
        };

        var options = {
            url: authSignupUrl,
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                Accept: 'application/json'
            },
            form: authData
        };

        request.post(options, function (err, authResult) {
            console.log('auth signup user err ' + err);
            console.log('auth signup user res ' + JSON.stringify(authResult));
            if (authResult) {
                user.save(function (error, result) {
                    if (error) {
                        console.log(error);
                        res.json('User is already exits');
                    }
                    else {
                        res.json({ Result: authResult });
                    }
                });
            } if (err) {
                res.json({ Error: 'Something went wrong' });
            }
        });

    }
    catch (error) {
        res.json({ Error: error });
    }
};
 */
