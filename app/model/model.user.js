/*jshint esversion: 6 */

const mongoose = require('mongoose');
const validator = require('validator');
const timestamps = require('mongoose-timestamp');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
mongoose.set('useFindAndModify', false);

var UserSchema = mongoose.Schema({
  name: {
    type: String,
    minlength: 1,
    required: true,
    maxlength: 50,
    trim: true,
  },
  email: {
    type: String,
    required: true,
    minlength: 1,
    maxlength: 50,
    trim: true,
    lowercase: true,
    unique: true,
    validate: value => {
      if (!validator.isEmail(value)) {
        throw new Error({ error: 'Invalid Email address' });
      }
    }
  },
  role: {
    type: Number,
    default: 0
  },
  isActive: {
    type: Boolean,
    required: true,
    default: false
  },

  /* dateCreated: {
    type: Date,
    default:Date
  },
  dateUpdated: {
    type: Date,
    default: Date.now
  } */
});

UserSchema.plugin(timestamps, {
  createdAt: 'dateCreated',
  updatedAt: 'dateUpdated'
});

UserSchema.plugin(AutoIncrement, { 
  inc_field: 'userid' 
});

module.exports = mongoose.model('user', UserSchema);
