/*jshint esversion: 6 */
const passport = require('passport');
const jwt = require('express-jwt');
const jwtAuthz = require('express-jwt-authz');
const jwksRsa = require('jwks-rsa');
const config = require('../../config/config');

// Authentication middleware. When used, the
// Access Token must exist and be verified against
// the Auth0 JSON Web Key Set
const DOMAIN = config.get('authentication.oauth0.domain');
const checkJwt = jwt({
    // Dynamically provide a signing key
    // based on the kid in the header and
    // the signing keys provided by the JWKS endpoint.
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: `https://dev-vibrant.auth0.com/.well-known/jwks.json`
    }),

    // Validate the audience and the issuer.
     audience: 'c5Qv8Y9eCbnDXiOaqS45CR9lmuIYrLGo',
    // Unique Identifier to access auth api
    //  audience:'https://dev-vibrant.auth0.com/api/v2/',
    issuer: `https://dev-vibrant.auth0.com/`,
    algorithms: ['RS256']
});

module.exports = (app) => {
    const routes = require('../controller/controller.user');
    const authRoutes = require('../controller/controller.auth');
    app.post('/UserSignup', routes.UserSignup);
    app.get('/UserList',checkJwt, routes.UserList);
    app.put('/UserUpdate', checkJwt,routes.UserUpdate);
    app.delete('/UserDelete/:userid', checkJwt, routes.UserDelete);
    app.post('/Userlogin',routes.Userlogin);
    app.post('/changePassword', routes.changePassword);
    app.get('/UserActive',routes.UserActive);
    app.post('/UserSearch',routes.UserSearch);

    
    app.get('/secured',  checkJwt, (req, res) => {
        res.json(`Successfully made a request for a secured endpoint by ${req.user.name}`);
    });
    app.get('/auth/auth0', passport.authenticate('auth0', {scope: 'openid email profile'}), authRoutes.oauth0Login);
    app.get('/auth/auth0/callback', passport.authenticate('auth0', { failureRedirect: '/auth/auth0' }), authRoutes.oauth0LoginCallback);

    //Error handling for token verification
    app.use(function (err, req, res, next) {
        if(err.status === 401 || err.name === 'UnauthorizedError') {
            res.status(401).send({ message:err.message});
        }
    });
};


//another way of Error handling for token verification
/* const authErrors = (err, req, res, next) => {
  if (err.name === "UnauthorizedError") {
    console.log(err); // You will still want to log the error...
    // but we don't want to send back internal operation details
    // like a stack trace to the client!
    res.status(err.status).json({ errors: [{ message: err.message }] });
    res.end();
  }
}; */

