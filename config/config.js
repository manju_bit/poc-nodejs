/*jshint esversion: 6 */
const convict = require("convict");

const config = convict({
  http: {
    port: {
      doc: "The port to listen on",
      default: 5000,
      env: "PORT"
    }
  },
  authentication: {
    oauth0: {
      domain: {
        doc: "Oauth0 domain",
        default: "dev-vibrant.auth0.com",
        env: "AUTH0_DOMAIN"
      },
      clientID: {
        doc: "Oauth0 clientID",
        default: "c5Qv8Y9eCbnDXiOaqS45CR9lmuIYrLGo",
        env: "AUTH0_CLIENT_ID"
      },
      clientSecret: {
        doc: "Oauth0 clientSecret",
        default:
          "kNswgIYNc2MpYbaYI2yfW1h1MCmxC2QXiQc8yLAv-0rBMSa_sOCapuZCjcM8CCi_",
        env: "AUTH0_CLIENT_SECRET"
      },
        dbConnection: {
        doc: "Application Database",
            default:"Username-Password-Authentication",
            env: "AUTH0_CONNECTION"
        },
      callbackURL: {
        doc: "Oauth0 callback URL",
        default: "/auth/auth0/callback"
      },
      passReqToCallback: {
        doc: "pass request to callback",
        default: true
      }
    }
  },
  database: {
    mongoDB: {
      url: {
        doc: "MongoDB URL",
        default: "mongodb://localhost:27017/userDB"
      }
    }
  }
});

config.validate();

module.exports = config;
