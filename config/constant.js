errorMsg = {

    LIST_LIMIT: 10,
    EMAIL_INVALID: 992,
    LOGIN_FAILED: 993,
    LIST_FAILED: 994,
    FIELD_REQ: 995,
    NO_DATA: 996,
    SUCCESS: 997,
    AUTH_ERROR: 998,
    EXCEPTION: 999,
    SIGNUP_USE_EMAIL: 1000,
    USER_EXIST: 1001,
    USER_NOT_UPDATE: 1002,
    UPDATE_USER: 1003,
    DELETE_USER: 1004,
    USER_NOT_DELETED: 1005,
   
    codes: {
        992: "Invalid email",
        993: "Wrong email and password",
        994: "Failed to list",
        995: "name field is required",
        996: "No data found",
        997: "Success",
        998: "Authentication error!",
        999: "Something went wrong!",
        1000: "Signup using another Email",
        1001: "User already exist",
        1002: "user is not updated",
        1003: "user updated successfully",
        1004: "user deleted successfully",
        1005: "user is not deleted",
        
    }

};

module.exports.getResponse =  function getResponse(code) {
    return errorMsg.codes[code];
};
module.exports.errorMsg = errorMsg;

