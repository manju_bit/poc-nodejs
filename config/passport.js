/*jshint esversion: 6 */

const passport = require('passport');
const passportJwt = require('passport-jwt');
const Auth0Strategy = require('passport-auth0');
const config = require('./config');

const User = require('../app/model/model.user');

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
        done(err, user);
    });
});

const auth0StrategyConfig = new Auth0Strategy({
        domain: config.get('authentication.oauth0.domain'),
        clientID: config.get('authentication.oauth0.clientID'),
        clientSecret: config.get('authentication.oauth0.clientSecret'),
        callbackURL: config.get('authentication.oauth0.callbackURL')
    },
    function (accessToken, refreshToken, extraParams, profile, done) {
        // accessToken is the token to call Auth0 API (not needed in the most cases)
        // extraParams.id_token has the JSON Web Token
        // profile has all the information from the user
        console.log(profile);
        const user = {
            name: profile.displayName,
            email: profile.emails[0].value
        };

        const userM = new User(user);
        userM.save();
        return done(null, profile, extraParams);
    }
);

passport.use('auth0', auth0StrategyConfig);
