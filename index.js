/*jshint esversion: 6 */

const express = require('express');
const session = require('express-session');
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const passport = require('passport');
const config = require('./config/config');

/**
 * API keys and Passport configuration.
 */
require('./config/passport');

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.config({ path: '.env.example' });

var app = express();
const MONGO_URI = config.get('database.mongoDB.url');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(passport.initialize());
app.use(passport.session());
app.use(
	session({
		resave: true,
		saveUninitialized: true,
		secret: 'session_secret',
		cookie: { maxAge: 1209600000 }, // two weeks in milliseconds
		store: new MongoStore({
			url: MONGO_URI,
			autoReconnect: true
		})
	})
);

mongoose
  .connect(MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log('Connected ');
  })
  .catch(err => {
    console.log('Not connected', err);
    process.exit();
  });
  app.use(function (req, res, next) {
	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE,PATCH");
	res.setHeader("Access-Control-Allow-Headers", "*");
	next();
});
require('./app/route/route.user')(app);
// PORT is another variable that can be placed in the .env file
const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log(`POC app listening on port ${port}!`);
});
module.exports = app;
