import { Component, OnInit, Input, SimpleChanges,Output, EventEmitter } from '@angular/core';

@Component({ 
  selector: 'app-button', 
  templateUrl: './button.component.html', 
  styleUrls: ['./button.component.css'] 
}) 

export class ButtonComponent implements OnInit {
   // @Input() message: string; 
   //text:any; 
   @Output() messageEvent = new EventEmitter<string>(); 
   /* btn:string; success:string="btn btn-success btn-sm"; 
   danger:string="btn btn-primary btn-lg"; */
 constructor() {

  } 
  ngOnInit() { 

  } 
  /* ngOnChanges(changes: SimpleChanges) {
     // this.text = changes; 
     let change = changes['message'];   
     if(this.message==="small"){ 
       this.btn=this.success; 
      }else if (this.message=== "large"){
         this.btn=this.danger; 
        } 
      } */ 
  sendMessage() { 
    this.messageEvent.emit(/* this.message */) 
  } 
}