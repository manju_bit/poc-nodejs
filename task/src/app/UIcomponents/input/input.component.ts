import { Component, OnInit,HostBinding, Output, EventEmitter, Input, forwardRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { FormGroup, NG_VALUE_ACCESSOR, NG_VALIDATORS, FormBuilder, ControlValueAccessor, Validators, FormControl } from '@angular/forms';


@Component({ 
  selector: 'app-input', 
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => InputComponent),
      multi: true,
    }]
})
  
  export class InputComponent implements OnInit {
   /*  private jsonString: string;
    private parseError: boolean;
    private data: any; */

    value:string;  
    onChange:()=>void;  
    onTouched:()=>void;  
    disabled:boolean;

  constructor(private formBuilder: FormBuilder) { 

  }

 ngOnInit() { 
 
 }

 writeValue(value:string){
       this.value=value? value:'';  
  }  

 registerOnChange(fn:any){
       this.onChange=fn;  
  }  
  registerOnTouched(fn:any){
        this.onTouched=fn;  
  }  
  setDisabledState(isDisable:boolean){
        this.disabled=isDisable;  
  }
 /* public writeValue(obj: any) {
  if (obj) {
      this.data = obj;
      this.jsonString = JSON.stringify(this.data, undefined, 4); 
  }
}

public registerOnChange(fn: any) {
  this.propagateChange = fn;
}

public validate(c: FormControl) {
  return (!this.parseError) ? null : {
      jsonParseError: {
          valid: false,
      },
  };
}

public registerOnTouched() { }

private onChange(event) {

  let newValue = event.target.value;

  try {
      this.data = JSON.parse(newValue);
      this.parseError = false;
  } catch (ex) {
      this.parseError = true;
  }

  this.propagateChange(this.data);
}

private propagateChange = (_: any) => { }; */
 
}