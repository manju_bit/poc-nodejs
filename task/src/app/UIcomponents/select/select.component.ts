import { Component, OnInit, forwardRef, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormBuilder, NG_VALUE_ACCESSOR, NG_VALIDATORS, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';

export interface ProfileFormValues {
  countryControl: string;
}

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => SelectComponent),
      multi: true,
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectComponent implements OnInit {
  form: FormGroup;
  countryControl:FormControl
  subscriptions: Subscription[] = [];
  countries = ['USA', 'Canada', 'Uk']

  get value(): ProfileFormValues {
    return this.form.value;
  }
  set value(value: ProfileFormValues) {
    this.form.setValue(value);
    this.onChange(value);
    this.onTouched();
  }

  constructor(private fb: FormBuilder) { 
    /* this.form = this.fb.group({
      countryControl: ['']
      
    });  */
    this.createFormControls();
    this.createLoginForm();
    this.subscriptions.push(
      this.form.valueChanges.subscribe(value => {
        this.onChange(value);
        this.onTouched();
      })
    );
  }

  ngOnInit() {
    /* this.form = this.fb.group({
      countryControl: ['']
    }); */
    
  }

  createFormControls() {
    this.countryControl = new FormControl('');
}

  createLoginForm() {
    this.form = new FormGroup({
      countryControl: this.countryControl,
    });
}
  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
  onChange: any = () => {};
  onTouched: any = () => {};

  registerOnChange(fn) {
    this.onChange = fn;
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }

    if (value === null) {
      this.form.reset();
    }
  }
  registerOnTouched(fn) {
    this.onTouched = fn;
  }
  validate(_: FormControl) {
    return this.form.valid ? null : { profile: { valid: false, }, };
  }

}
