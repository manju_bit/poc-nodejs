import { Component, OnInit,Input, forwardRef, ChangeDetectionStrategy,HostBinding } from '@angular/core';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, FormGroup, FormBuilder, FormControl, ControlValueAccessor } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-text-area',
  templateUrl: './text-area.component.html',
  styleUrls: ['./text-area.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextAreaComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => TextAreaComponent),
      multi: true,
    }] 
})
export class TextAreaComponent implements OnInit,ControlValueAccessor {
  private jsonString: string;
  private parseError: boolean;
  private data: any;

  constructor(private formBuilder: FormBuilder) { 
    
  }

  public writeValue(obj: any) {
    if (obj) {
        this.data = obj;
        // this will format it with 4 character spacing
        this.jsonString = JSON.stringify(this.data); 
    }
}

public registerOnChange(fn: any) {
  this.propagateChange = fn;
}
public validate(c: FormControl) {
  return (!this.parseError) ? null : {
      jsonParseError: {
          valid: false,
      },
  };
}

public registerOnTouched() { }

private onChange(event) {
      
  let newValue = event.target.value;

  try {
      this.data = JSON.parse(newValue);
      this.parseError = false;
  } catch (ex) {
      this.parseError = true;
  }
  this.propagateChange(this.data);
  // update the form
}

  ngOnInit() {
  }

  private propagateChange = (_: any) => { };

}
