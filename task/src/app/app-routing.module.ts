import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './shared/commonComponents/home/home.component';
import { DashboardComponent } from './shared/commonComponents/dashboard/dashboard.component';
import { AuthGuard } from './core/guards/auth.guard';
import { AuthorizationGuard } from './core/guards/authorization';

const routes: Routes = [
  {
    path: 'authendication',
    loadChildren: './modules/auth/auth.module#AuthModule'
}, 
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' }, 
{
  path: '',
  component: HomeComponent,
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
       canActivate: [AuthGuard,AuthorizationGuard]
  },
  {
    path: '',
    loadChildren: './modules/user/users.module#UsersModule',
     canActivate: [AuthGuard, AuthorizationGuard] 
  }, 
  ]
} 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
