import { Component, ViewChild,AfterViewInit, ElementRef, OnInit, ChangeDetectionStrategy  } from '@angular/core';
//import { IN } from "./UIcomponents";
 //import {ButtonComponent} from './UIcomponents/index';
 import { ButtonComponent} from './UIcomponents/button/button.component';
 import { InputComponent} from './UIcomponents/input/input.component';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import * as Highcharts from 'highcharts';
import Heatmap from 'highcharts/modules/heatmap.js';
Heatmap(Highcharts);


export class Hero {
  id: number;
  name: string;
}

 @Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit{
  Highcharts = Highcharts;
  heatMapOptions={}

  myForm: FormGroup
  value = false;
  public result = {};
  public mySwitch:boolean;
  

  constructor(private formBuilder: FormBuilder) {
   
    this.result = {"hello":"world"};
    
    this.myForm = this.formBuilder.group({
      result: [{"test123":"test456"}],
      mySwitch: [true],
      name: [{"name":"show"}]
    })
  }

  ngOnInit() { 
    /* this.myForm = this.formBuilder.group({
      mySwitch: [true]
    }); */
    /* console.log(this.btntext)  */
    this.heatMap();
    /* this.myForm = this.formBuilder.group({
      mySwitch: [true]
    }); */
  } 
  heatMap(){
    this.heatMapOptions = {   
     chart: {
     type: 'heatmap',
     marginTop: 40,
     marginBottom: 80,
     plotBorderWidth: 0
 },


 title: {
     text: ''
 },

 xAxis: {
     categories: ['Alexander', 'Marie', 'Maximilian', 'Sophia', 'Lukas', 'Maria', 'Leon', 'Anna', 'Tim', 'Laura'],
     opposite: true,
 },

 yAxis: {
     categories: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
     title: null,
     reversed: true
 },

 colorAxis: {
     min: 0,
     minColor: '#FFFFFF',
     maxColor: '#3a4ff0'
 },

 legend: {
     align: 'right',
     layout: 'vertical',
     margin: 0,
     verticalAlign: 'top',
     y: 25,
     symbolHeight: 280,
     enabled: false
 },

 tooltip: {
     formatter: function () {
         return '<b>' + this.series.xAxis.categories[this.point.x] + '</b> sold <br><b>' +
             this.point.value + '</b> items on <br><b>' + this.series.yAxis.categories[this.point.y] + '</b>';
     }
 },

 series: [{
     name: 'Sales per employee',
     borderWidth: 1,
     borderColor: 'white',
     data: [[0, 0, 10], [0, 1, 19], [0, 2, 8], [0, 3, 24], [0, 4, 67], [1, 0, 92], [1, 1, 58], [1, 2, 78], [1, 3, 117], [1, 4, 48], [2, 0, 35], [2, 1, 15], [2, 2, 123], [2, 3, 64], [2, 4, 52], [3, 0, 72], [3, 1, 132], [3, 2, 114], [3, 3, 19], [3, 4, 16], [4, 0, 38], [4, 1, 5], [4, 2, 8], [4, 3, 117], [4, 4, 115], [5, 0, 88], [5, 1, 32], [5, 2, 12], [5, 3, 6], [5, 4, 120], [6, 0, 13], [6, 1, 44], [6, 2, 88], [6, 3, 98], [6, 4, 96], [7, 0, 31], [7, 1, 1], [7, 2, 82], [7, 3, 32], [7, 4, 30], [8, 0, 85], [8, 1, 97], [8, 2, 123], [8, 3, 64], [8, 4, 84], [9, 0, 47], [9, 1, 114], [9, 2, 31], [9, 3, 48], [9, 4, 91]],
     dataLabels: {
         enabled: false,
         color: '#000000',
     }
 }],

 responsive: {
     rules: [{
         condition: {
             maxWidth: 500
         },
         chartOptions: {
             yAxis: {
                 labels: {
                     formatter: function () {
                         return this.value.charAt(0);
                     }
                 }
             }
         }
     }]
 }
}
}
 


  submit() {
    console.log(`Value: ${this.myForm.controls.mySwitch.value} `);
    console.log(`text: ${this.myForm.controls.result.value.test123} `);
    console.log(`input: ${this.myForm.controls.name.value.name} `);
  }

  
  
}
