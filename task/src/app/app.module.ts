import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InputComponent } from './UIcomponents/input/input.component';
import { ButtonComponent } from './UIcomponents/button/button.component';
import { SelectComponent } from './UIcomponents/select/select.component';
import { RadioComponent } from './UIcomponents/radio/radio.component';
import { TextAreaComponent } from './UIcomponents/text-area/text-area.component';
import { CheckBoxComponent } from './UIcomponents/check-box/check-box.component';
import { CommonComponentModule } from './shared/commonComponents/common-component.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { DataTablesModule } from 'angular-datatables';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SessionService } from './core/services/session.service';
import { HighchartsChartModule } from 'highcharts-angular';

import { CoreModule } from './core/core.module';

@NgModule({
  declarations: [
    AppComponent,
    InputComponent,
    ButtonComponent,
    SelectComponent,
    RadioComponent,
    TextAreaComponent,
    CheckBoxComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CommonComponentModule,
    MDBBootstrapModule.forRoot(),
    DataTablesModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    HighchartsChartModule
    //CoreModule.forRoot(), 
  ],
  providers: [SessionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
