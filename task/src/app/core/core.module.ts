 import { NgModule, ModuleWithProviders } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';


import { SessionService } from './services/session.service';
import { AuthModule } from '../modules/auth/auth.module';
import { HeaderInterceptor } from './interceptors/interceptor';

@NgModule({
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HeaderInterceptor,
            multi: true
        },
        SessionService
        ],
    imports: [AuthModule]
})
export class CoreModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule
        };
    }
}
 