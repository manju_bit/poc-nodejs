import { Injectable, OnInit } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from '../services/session.service';
import { AuthendicationService } from 'src/app/shared/services/authendication.service';
import { ToasterService } from 'src/app/shared/services/toaster.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, OnInit {

  constructor(  
    private auth: AuthendicationService,
    private router: Router,
    private sessionService: SessionService,
    private toaster:ToasterService
    ) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.sessionService.getSession()) {
      this.router.navigate(['', 'authendication', 'login']);
      return false;
  }  else if (this.sessionService.getSession() && this.auth.isTokenExpired() ) {
        this.toaster.error('Token expired.Please login');
        this.router.navigate(['', 'authendication', 'login']);
    return false;
} 
return true;
}
ngOnInit(): void {}
}
