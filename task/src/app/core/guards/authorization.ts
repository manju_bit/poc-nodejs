import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AuthendicationService } from 'src/app/shared/services/authendication.service';
import { SessionService } from '../services/session.service';



@Injectable({
    providedIn: 'root'
})
export class AuthorizationGuard implements CanActivate {
    constructor(private auth: AuthendicationService,private router: Router, private sessionService: SessionService) {}

    canActivate(route: ActivatedRouteSnapshot): boolean {
        if (this.auth.isTokenExpired()) {
            this.router.navigate(['', 'authendication', 'login']);
            return false;
        }
        
        
        return true;
    }
}
