import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { SessionService } from '..';
import { catchError, tap } from 'rxjs/operators';
//import { AuthenticationService, EncDecrService, ToasterService } from '../../shared';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class HeaderInterceptor implements HttpInterceptor {
    constructor(
        private sessionService: SessionService,
    ) {}

  

    private static handleError(error: HttpErrorResponse) {
        let errMsg = '';
        if (error.error instanceof ErrorEvent) {
            errMsg = `Error: ${error.error.message}`;
        } else {
            if (error.status === 400 || error.status === 401) {
            }
            errMsg = `Error Code: ${error.status},  Message: ${error.message}`;
        }
        return throwError(errMsg);
       
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const headers = {
            setHeaders: {}
        };

        if (!req.headers.has('Content-Type')) {
            headers.setHeaders['Content-Type'] = 'application/json';
        }

          if (this.sessionService.getJwtToken()) {
            headers.setHeaders['Authorization'] = 'crud ' + this.sessionService.getJwtToken();
        }  

      

        const authReq = req.clone(headers);
        return ;
    }
}
