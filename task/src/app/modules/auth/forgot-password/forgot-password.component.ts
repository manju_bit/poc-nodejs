import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CONSTANTS } from 'src/app/shared/constant';
import { LoginService } from 'src/app/shared/services/login.service';
import { ToasterService } from 'src/app/shared/services/toaster.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  loginForm: FormGroup;
  email: FormControl;

  isFormSubmitted = false;
  constructor(private loginService: LoginService,private toater:ToasterService,private router: Router,) { }

  ngOnInit() {
    this.createFormControls();
    this.createLoginForm();
  }
  createFormControls() {
    this.email = new FormControl('', [Validators.required, Validators.pattern(CONSTANTS.regex.email)]);
    
}
createLoginForm() {
  this.loginForm = new FormGroup({
      email: this.email,
  });
}

login() {
  if (this.loginForm.valid) {
      this.isFormSubmitted = false;
        this.loginService.forgot(this.loginForm.value).subscribe((res:any)=>{
          if(res.success)
          this.toater.showSuccess(res.success.message);
          //this.router.navigate(['www.google.com']);
          window.open("http://www.gmail.com")
        })
  } else {
      this.isFormSubmitted = true;
  }
}

}
