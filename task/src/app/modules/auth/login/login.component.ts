import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CONSTANTS } from 'src/app/shared/constant';
import { Router } from '@angular/router';
import { ToasterService } from 'src/app/shared/services/toaster.service';
import { LoginService } from 'src/app/shared/services/login.service';
import { SessionService } from 'src/app/core/services/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  email: FormControl;
  password: FormControl;

  passwordName:boolean=false;
  buttonName:any = "password";

  isFormSubmitted = false;

  constructor(
    private router: Router,
    private toater:ToasterService,
    private loginService:LoginService,
    private session:SessionService
     ) { }

  ngOnInit() {
    this.loginFormControls();
    this.createLoginForm();
    
  }

  loginFormControls() {
    this.email = new FormControl('', [Validators.required, Validators.pattern(CONSTANTS.regex.email)]);
    this.password = new FormControl('', [
        Validators.required,
        Validators.minLength(CONSTANTS.regex.passwordMinLength),
        Validators.maxLength(CONSTANTS.regex.passwordMaxLength)
    ]);
}

createLoginForm() {
    this.loginForm = new FormGroup({
        email: this.email,
        password: this.password
    });
}

login() {
  if (this.loginForm.valid) {
      this.isFormSubmitted = false;
        this.loginService.login(this.loginForm.value).subscribe((res:any)=>{
          if(res.Error){
          this.toater.error(res.Error);
        } else if (res) {
          this.toater.showSuccess('You are successfully logged in');
          this.session.setSession(res);
          this.router.navigate(['', 'dashboard']);
      }
        },
        error=>{
          this.toater.error("something went wrong");
        }
        )
  } else {
      this.isFormSubmitted = true;
  }
}
toggle(){
  this.passwordName = !this.passwordName;
  if(this.passwordName){
    this.buttonName="text"
  }else{
    this.buttonName="password"
  }
}

}
