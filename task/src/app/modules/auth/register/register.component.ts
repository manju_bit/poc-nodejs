import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CONSTANTS } from 'src/app/shared/constant';
import { RegisterService } from 'src/app/shared/services/register.service';
import { ToasterService } from 'src/app/shared/services/toaster.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  RegisterForm: FormGroup;
  name:FormControl;
  email: FormControl;
  password: FormControl;
  passwordName:boolean=false;
  icons:any="fa fa-eye"
  buttonName:any = "password";

  isFormSubmitted = false;

  constructor(private registerService: RegisterService, private toater: ToasterService, private router: Router) { }

  ngOnInit() {
    this.registerFormControls();
    this.registerForm();
  }

  registerFormControls() {
    this.name = new FormControl('', [Validators.required, Validators.pattern(CONSTANTS.regex.name)]);
    this.email = new FormControl('', [Validators.required, Validators.pattern(CONSTANTS.regex.email)]);
    this.password = new FormControl('', [
        Validators.required,
        Validators.minLength(CONSTANTS.regex.passwordMinLength),
        Validators.maxLength(CONSTANTS.regex.passwordMaxLength)
    ]);
}

registerForm() {
  this.RegisterForm = new FormGroup({
      name: this.name,
      email: this.email,
      password: this.password
  });
}

Register() {
  if (this.RegisterForm.valid) {
      
      this.isFormSubmitted = false;
         this.registerService.Registers(this.RegisterForm.value).subscribe((res:any)=>{
           console.log(res)
           if(res){
            this.toater.showSuccess('Successfully registered an account');
            this.router.navigate(['', 'authendication', 'login']);
           }else{
            this.toater.showSuccess('Something went wrong');
           }
        }) 
  } else {
      this.isFormSubmitted = true;
  }
}

toggle(){
  this.passwordName = !this.passwordName;
  if(this.passwordName){
    this.buttonName="text"
    this.icons="fa fa-eye-slash"
  }else{
    this.buttonName="password"
    this.icons="fa fa-eye"
  }
}

}
