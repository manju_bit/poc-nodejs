import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CONSTANTS } from 'src/app/shared/constant';
import { ActivatedRoute, Router } from '@angular/router';
import { UserlistService } from 'src/app/shared/services/userlist.service';
import { ToasterService } from 'src/app/shared/services/toaster.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  editForm: FormGroup;
  name: FormControl;
  email: FormControl;
  password: FormControl;
  confirmPassword: FormControl;
  id;
  data;
  isFormSubmitted;
  format;



  constructor(
    private route: ActivatedRoute,
    private userService: UserlistService,
    private customToasterService: ToasterService,
    private router: Router
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.editFormControls();
    this.editForms();
    /* this.editForm.controls['name'].disable();*/
    //this.editForm.controls['email'].disable(); 
  }

  editFormControls() {
    this.userService.getUser().subscribe((pagedData: any) => {
      this.data = pagedData;
      var i;
      for (i = 0; i < pagedData.Users.length; i++) {
        var name = pagedData.Users[i].userid;
        this.format = name.toString();
        if (this.format === this.id) {
          this.editForm.controls.name.setValue(pagedData.Users[i].name);
          this.editForm.controls.email.setValue(pagedData.Users[i].email)
        }

      }

    })
    this.name = new FormControl('', [Validators.required, Validators.pattern(CONSTANTS.regex.username)]);
    this.email = new FormControl('', [Validators.required, Validators.pattern(CONSTANTS.regex.email)]);
  }


  editForms() {
    this.editForm = new FormGroup({
      name: this.name,
      email: this.email,
    });
  }

  edit() {
    if (this.editForm.valid) {
      this.isFormSubmitted = false;

      this.userService.updateUser(this.editForm.value, this.id).subscribe(
        (res: any) => {
          console.log(res);
          if (res.success === "user updated successfully") {
            this.customToasterService.showSuccess(res.success);
            this.router.navigate(['', 'users']);
          } else if (res.Error === "No access") {
            this.customToasterService.error(res.Error);
          } else if (res.Error === "no changes") {
            this.customToasterService.warning(res.Error);
          }
        },
        error => {
           this.customToasterService.error(error);
        })

    } else {
      this.isFormSubmitted = true;
    }
  }

}
