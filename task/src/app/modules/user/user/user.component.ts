import { Component, OnInit,  ViewChildren, QueryList, AfterViewInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import Swal from 'sweetalert2'
import { Subject } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { UserlistService } from 'src/app/shared/services/userlist.service';
import { ToasterService } from 'src/app/shared/services/toaster.service';
import { catchError } from 'rxjs/operators';

class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
}

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, AfterViewInit, OnDestroy {

  getUserlist:any=[];
  @ViewChildren(DataTableDirective)
  datatableElement: DataTableDirective;
  dtElements: QueryList<any>;
  dtTrigger: Subject<any> = new Subject();
  pageIndex = 0;
  pageLength = 10;
  dtOptions: any = {};

  constructor(private userService: UserlistService,private customToasterService: ToasterService,private cdr:ChangeDetectorRef) { }

  ngAfterViewInit() {
    this.dtTrigger.next();
}

  ngOnInit() {
    this.gridBinding();
  }

  gridBinding() {
     this.dtOptions[0] = {
      pagingType: 'full_numbers',
      pageLength: 100,
      serverSide: true,
      processing: true,
      order: [], 
      ajax: (dataTablesParameters: any, callback) => {
        //this.initializeTableIndex(dataTablesParameters); 
       dataTablesParameters.draw = this.pageIndex + 1;
      dataTablesParameters.length = this.pageLength; 
        this.userService.getUser().subscribe((pagedData: any) => {
          if (pagedData) {
              this.getUserlist = pagedData.Users; 
              callback({
                  recordsTotal: pagedData.Users.length,
                  recordsFiltered: pagedData.Users.length,
                  data: [] 
              });
          } else {
              this.getUserlist = [];
              callback({
                  recordsTotal: 0,
                  recordsFiltered: 0,
                  data: []
              });
          }
          this.cdr.detectChanges();
      });
      },
      columns: [
        { data: 'email', orderable: true  }, 
        { data: 'name', orderable: true  }, 
        { data: 'dateCreated', orderable: true  },
        { data: 'dateUpdated', orderable: true  },
        { data: 'actions', orderable: false  }
      ]
    }; 
}

ngOnDestroy(): void {
  // Do not forget to unsubscribe the event
  this.dtTrigger.unsubscribe();
}

deleteUser(user){
  Swal.fire({
    title: 'Are you sure?',
    text: 'You will not be able to recover this data!',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, keep it'
  }).then((result) => {
    if (result.value) {
      this.userService.deleteUser(user.userid).subscribe(
        res=>{
          if(res.message === "No access"){
            Swal.fire(
              res.message,
              'Your imaginary file is safe :)',
              'error'
            )
          }
      },
      error=>{
        console.log(error);
        Swal.fire(
          error,
          'Your imaginary file is safe :)',
          'error'
        )
      })
      
      
    } else if (result.dismiss === Swal.DismissReason.cancel) {
      Swal.fire(
        'Cancelled',
        'Your imaginary file is safe :)',
        'error'
      )
    }
  })
}
 

}
