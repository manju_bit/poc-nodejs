import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user/user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [UserComponent, EditUserComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    DataTablesModule,
    HttpClientModule,
    ReactiveFormsModule,
    ToastrModule.forRoot()
  ]
})
export class UsersModule { }
