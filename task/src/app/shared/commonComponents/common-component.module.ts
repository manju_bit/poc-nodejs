import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { HighchartsChartModule } from 'highcharts-angular';


@NgModule({
  declarations: [DashboardComponent,TopNavComponent,HomeComponent, SidenavComponent],
  imports: [
    CommonModule,
    RouterModule,
    HighchartsChartModule
  ]
})
export class CommonComponentModule { }
