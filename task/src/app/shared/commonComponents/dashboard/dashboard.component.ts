import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as Highcharts from 'highcharts';
import { DashboardService } from '../../services/dashboard.service';
import { interval, Subscription } from 'rxjs';
import { SessionService } from 'src/app/core';
import { AuthendicationService } from '../../services/authendication.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  active;
  inActive;
  total;
  Highcharts = Highcharts;
  chartOptions = {}
  donutChartOptions ={}
  columnChartOptions={}
  updateBillingChart = false;
  billingoneToOneFlag: boolean = false;

  activelength={}
  inactivelength={}
  totalactivelength={}
  length=[]

  constructor(private dashboardService: DashboardService,private cdr:ChangeDetectorRef,private auth:AuthendicationService) { }

 

  ngOnInit() {
   this.dashboardService.getCharts().subscribe((res:any)=>{
    this.active=res.users.active.length;
    this.inActive=res.users.inActive.length;
    this.total=this.active+this.inActive;
    this.donutChart() ;
    this.updateBillingChart = true;
    this.billingoneToOneFlag = true;
    /* console.log(this.active)
    

    let datas=this.donutChartOptions['series'][0].data;
    this.activelength={ name: 'Active', y: this.active },
    this.inactivelength={ name: 'inactive', y: this.inActive },
    this.totalactivelength={ name: 'total', y: this.total }, */
    
    //this.length.push(this.donutChartOptions['series'][0].data)
    /* datas.push(this.activelength);
    datas.push(this.inactivelength);
    datas.push(this.totalactivelength);
    console.log(datas) */

   /*  const updated_normal_data = [];
    const updated_abnormal_data = [];
    res.users.active.forEach(row => {
        const temp_row = [
          new Date(row.timestamp).getTime(),
          row.value
        ];
        row.Normal === 1 ? updated_normal_data.push(temp_row) : updated_abnormal_data.push(temp_row);
      });
      this.donutChartOptions['series'][0].data = updated_normal_data; */
        
    
    
    this.cdr.detectChanges();
});
  }

  donutChart(){
    this.donutChartOptions={
chart: {
       type: 'pie',
       options3d: {
           enabled: true,
           alpha: 45
       }
   },
   title: {
       text: ''
   },
   credits: {
    enabled: false
},
   subtitle: {
       text: ''
   },
   plotOptions: {
       pie: {
           innerSize: 0,
           depth: 45,
           showInLegend: true
       },
       
   },
   series: [{
       name: 'Users',
       data: [
        { name: 'Active users', y: this.active },
        { name: 'Inactive users', y: this.inActive },
        { name: 'Total users', y: this.total },
       ]
   }]
}
}

}
