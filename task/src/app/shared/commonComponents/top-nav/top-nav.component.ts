import { Component, OnInit } from '@angular/core';
import { ToasterService } from '../../services/toaster.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/core';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.css']
})
export class TopNavComponent implements OnInit {

  constructor(
    private toaster: ToasterService,
    private router:Router,
    private session:SessionService
    ) { }

  ngOnInit() {
  }

  logout(){
    this.session.clearSession();
    this.toaster.showSuccess("successfully logged out");
    this.router.navigate(['', 'authendication', 'login']); 
  }

}
