export const CONSTANTS = {
    regex: {
        email: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        phone: /^\+(?:[0-9] ?){6,14}[0-9]$/,
        name: /^(?![0-9]*$)[a-zA-Z0-9]+$/,
        price: /^\d+.\d{0,2}$/,
        passwordMinLength: 8,
        passwordMaxLength: 32,
        confirmpassword: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,})/,
        username: /^(?![0-9_]*$)[a-zA-Z0-9_]+$/,
        quantity: /[0-9]/,
        firstName: /^[a-zA-Z ]*$/,
        integer: /^(0|[1-9][0-9]*)$/,
        allowAlphaNumeric: /^[a-zA-Z0-9-]+$/,
        preventSingleQuote: /^[^\"\']*$/,
        preventSpecialChar: /^[a-zA-Z0-9,.\s]+$/
    }
};
