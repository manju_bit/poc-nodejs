import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, of as observableOf } from 'rxjs';
import * as jwt_decode from 'jwt-decode';


@Injectable({
  providedIn: 'root'
})
export class AuthendicationService {

  constructor() {

  }


  public isTokenExpired() {
    const helper = new JwtHelperService();
    const jwtRawToken = this.getJwtToken();
    if (jwtRawToken) {
      const decodeToken = helper.decodeToken(jwtRawToken);
      const expTime = decodeToken.exp * 1000;
      const currentTime = new Date().getTime();
      /* if(currentTime < expTime){
        throw new Error('Token not yet valid');
      }*/
      console.log( currentTime);
      if( currentTime > expTime) {
        //throw new Error('Token has expired');
        return true
      } 
      //return currentTime > expTime;
    }
    //return true;
  }
 
  private getJwtToken() {
    const item = localStorage.getItem('crud');
    const items = JSON.parse(item);
    if (items.idToken) {
      return items.idToken;
    }
    return null;
  }
}
