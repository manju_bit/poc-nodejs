import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { EncryptService } from './encrypt.service';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  Bearer = "Bearer";
  token;

  constructor(private http: HttpClient, private cryptoService: EncryptService) { }

  post(url: string, options?: any, header?: object): Observable<any> {
    const decData = options;
    return this.http.post(url, decData).pipe(
      map((res: any) => {
        if (res && res.success) {
          const data: any = this.cryptoService.decryptData(res.data);
          res.data = data;
        }
        return res;
      }),
      catchError((error: HttpErrorResponse) => this.err(error))
    );
  }

  put(url: string, options?: any, id?: any) {
    this.getJwtToken()
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('userid', id);
    headers = headers.append('Authorization', this.Bearer + " " + this.token);
    let header = { headers: headers };

    const decData = options;
    return this.http.put(url, decData, header).pipe(
      map((res: any) => {
        if (res && res.success) {

        }
        return res;
      }),
      catchError((error: HttpErrorResponse) => this.err(error))
    )

  }

  delete(url: string, options?: any, header?: object): Observable<any> {
    const decData = options;
    const tempoptions = {
      headers: new HttpHeaders({}),
      body: decData
    };
    return this.http.delete(url, tempoptions).pipe(
      map((res: any) => {
        if (res && res.success) {
          const data: any = this.cryptoService.decryptData(res.data);
          res.data = data;
        }
        return res;
      }),
      catchError((error: HttpErrorResponse) => this.err(error))
    );
  }


  err(error: HttpErrorResponse) {
    return throwError(error.message);
  }

  getJwtToken() {
    const item = localStorage.getItem('crud');
    const items = JSON.parse(item)
    if (items.idToken) {
      this.token = items.idToken;
    }
    //return null;
  }


}

