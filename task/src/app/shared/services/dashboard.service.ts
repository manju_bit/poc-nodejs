import { Injectable } from '@angular/core';
import { EndpointService } from './endpoint.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private endpointService: EndpointService,private http:HttpClient) { }

  getCharts(){
    return this.http.get(this.endpointService.userChart());
  }
}
