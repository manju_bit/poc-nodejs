import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EndpointService {

  constructor() { }

  //user Url

  userList = this.getBaseUrl() + 'UserList';
  editUser = this.getBaseUrl() + 'UserUpdate';
  deleteUser = this.getBaseUrl() + 'UserDelete/:id';

  //login Url
  loginUrl = this.getBaseUrl() + 'Userlogin';

  //register Url
  register = this.getBaseUrl() + 'UserSignup';

  //forgor password
  forgotUrl = this.getBaseUrl() + 'changePassword';

  //for chart
  charts = this.getBaseUrl() + 'UserActive';


  getBaseUrl() {
    return environment.host;
  }

  getUserList() {
    return this.userList;
  }
  UserEdit() {
    return this.editUser;
  }
  deleteUsers() {
    return this.deleteUser;
  }

  userLogin() {
    return this.loginUrl;
  }

  userRegister() {
    return this.register;
  }

  userForgot() {
    return this.forgotUrl
  }

  userChart() {
    return this.charts
  }
}
