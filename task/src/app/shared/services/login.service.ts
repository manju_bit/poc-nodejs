import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { EndpointService } from './endpoint.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private baseService: BaseService, private endpointService: EndpointService) { }

  login(body){
    return this.baseService.post(this.endpointService.userLogin(),body);
  }

  forgot(body){
    return this.baseService.post(this.endpointService.userForgot(),body)
  }

}
