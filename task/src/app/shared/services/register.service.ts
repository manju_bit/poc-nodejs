import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { EndpointService } from './endpoint.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private baseService: BaseService,private endpointService:EndpointService) { }

  Registers(body){
    return this.baseService.post(this.endpointService.userRegister(),body)
  }

}
