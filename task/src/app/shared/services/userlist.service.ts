import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { EndpointService } from './endpoint.service';
import { BaseService } from './base.service';
import { Observable, throwError } from 'rxjs';
import { Headers } from '@angular/http';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserlistService {

  Bearer = "Bearer";
  token;

  constructor(
    private http: HttpClient,
    private endpointservice: EndpointService,
    private baseService: BaseService) { }

  getUser() {
    this.getJwtToken();
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', this.Bearer + " " + this.token);
    let options = { headers: headers };
    return this.http.get(this.endpointservice.getUserList(), options);
  }

  /*  updateUser(body:User,id) {
     let headers = new HttpHeaders();
     headers=headers.append('Content-Type', 'application/json');
     headers=headers.append('userid', id);
     let options = { headers: headers };
   
     const putUrl = this.endpointservice.UserEdit() ;
     return this.http.put(putUrl, body, options);
 } */

  updateUser(body, id) {
    return this.baseService.put(this.endpointservice.UserEdit(), body, id)
  }

  deleteUser(id) {
    this.getJwtToken()
    const url = "http://192.168.1.122:5000/UserDelete";
    var userid = id;
    var baseurl = `${url}/${userid}`
    let headers = new HttpHeaders();
    /* headers=headers.append('Content-Type', 'application/json'); */
    headers = headers.append('Authorization', this.Bearer + " " + this.token);
    let options = { headers: headers };
    /* return this.baseService.delete(`${this.endpointservice.deleteUsers()}`,id) */
    return this.http.delete(baseurl, options).pipe(
      map((res: any) => {
        return res;
      }),
      catchError((error: HttpErrorResponse) => this.err(error.error.Error))
    );
  }

  getJwtToken() {
    const item = localStorage.getItem('crud');
    const items = JSON.parse(item)
    if (items.idToken) {
      this.token = items.idToken;
    }
    //return null;
  }
  err(error: HttpErrorResponse) {
    return throwError(error);
  }


}
